using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPickUp : MonoBehaviour
{
    // Start is called before the first frame update
   // [SerializeField] private Camera cam;
    private float pickUpRange = 2f;
    private PickUpController controller;
    GameObject obj;
    [SerializeField] private GameObject pickGunText;
    public Material newMtl;
    Material oldMtl;

    void Start()
    {
        controller = GetComponent<PickUpController>();
        pickGunText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * pickUpRange, Color.red);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, pickUpRange))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Gun"))
            {
                obj = hit.collider.gameObject; 
                oldMtl = hit.collider.GetComponent<MeshRenderer>().material; 
                hit.collider.GetComponent<MeshRenderer>().material = newMtl;
                pickGunText.SetActive(true);
                if(Input.touchCount < 0)
                {
                    controller.PickUp();
                }
            }
        }
        else
        {
            pickGunText.SetActive(false);
            if (obj != null)
            {
                obj.GetComponent<MeshRenderer>().material = null;
                obj = null;
            }
        }
    }
}
