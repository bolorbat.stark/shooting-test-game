using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Scriptable scriptableObject;
    private PickUpController controller;
    private int ammo = 1;

    private void Start()
    {
        controller = GetComponent<PickUpController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(Input.GetKeyDown(KeyCode.E) && controller.equipped)
        {
            scriptableObject.ammo += ammo;
            Destroy(gameObject);
        }
    }
}
