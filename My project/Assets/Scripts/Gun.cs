using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Gun : MonoBehaviour
{
    [SerializeField] private Scriptable gunData;
    [SerializeField] private Transform muzzle;
    private float range = 50f;
    private PickUpController controller;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<PickUpController>();

    }
    private void Update()
    {
        Debug.DrawRay(muzzle.transform.position, muzzle.transform.forward * range, Color.red);

    }
    public void Shoot()
    {
        Debug.Log("in");
        if (controller.equipped && A.IsClick && gunData.ammo < 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(muzzle.transform.position, muzzle.transform.forward, out hit, range))
            {
                Debug.Log(hit.transform.name);
            }
            gunData.ammo--;
        }
    }
}
