using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private PlayerInputActions action; 
    private PlayerMotor motor;
    private PlayerLook playerLook;
    [SerializeField] private Gun gun;


    // Start is called before the first frame update
    void Awake()
    {
        action = new PlayerInputActions();
        playerLook = GetComponent<PlayerLook>();
        motor = GetComponent<PlayerMotor>();
        action.PlayerMain.Jump.performed += ctx => motor.Jump();
        action.PlayerMain.Shoot.performed += _ => gun.Shoot();
    }
   
    private void OnEnable()
    {
        action.Enable();
    }
    private void OnDisable()
    {
        action.Disable();
    }

}
