using System;
using UnityEngine;

#if UNITY_IOS
using UnityEngine.iOS;
using Unity.Advertisement.IosSupport;
#endif

public delegate void OnAttributionFinished();

public class ATTHandler : MonoBehaviour
{
#if UNITY_IOS
    OnAttributionFinished continueAction;
    static bool IsAttAuthorizationRequired => ATTrackingStatusBinding.GetAuthorizationTrackingStatus() == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED;
    static ATTrackingStatusBinding.AuthorizationTrackingStatus AttAuthorizationStatus => ATTrackingStatusBinding.GetAuthorizationTrackingStatus();

    public void CheckTrackingRequest(OnAttributionFinished action)
    {
        continueAction = action;
        var status = ATTrackingStatusBinding.GetAuthorizationTrackingStatus();
        Version currentVersion = new Version(Device.systemVersion);
        Version ios14 = new Version("14.5");

        if (IsAttAuthorizationRequired && currentVersion >= ios14) {
            ATTrackingStatusBinding.RequestAuthorizationTracking(OnAuthorizationTrackingRecieved);
        } else {
            continueAction?.Invoke();
            return;
        }
    }

    void OnAuthorizationTrackingRecieved(int status)
    {
        if (AttAuthorizationStatus == ATTrackingStatusBinding.AuthorizationTrackingStatus.AUTHORIZED)
            SkAdNetworkBinding.SkAdNetworkRegisterAppForNetworkAttribution();
        continueAction?.Invoke();
    }
#endif
}
