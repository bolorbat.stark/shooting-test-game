using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PickUpController : MonoBehaviour
{
    public Transform player, gunContainer, cam;
    public Rigidbody rb;
    public float pickUpRange;
    public float dropWardForce;
   

    public bool equipped;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 distanceToPlayer = transform.position - player.position;

        if (!equipped && distanceToPlayer.magnitude <= pickUpRange && Input.GetKeyDown(KeyCode.E)) PickUp();

        if(equipped && Input.GetKeyDown(KeyCode.G)) Drop(); 
        
    }

    private void Drop()
    {
        transform.SetParent(null);

        rb.velocity = player.GetComponent<CharacterController>().velocity;

        rb.AddForce(cam.forward * dropWardForce, ForceMode.Impulse);
        rb.AddForce(cam.up * dropWardForce, ForceMode.Impulse);

        float random = UnityEngine.Random.Range(-1, 1);
        rb.AddTorque(new Vector3(random,random,random));

        rb.isKinematic = false;
        equipped = false;
    }

    public void PickUp()
    {
        equipped = true;    
        rb.isKinematic = true;

        transform.SetParent(gunContainer);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0,90,0);
    }
}
