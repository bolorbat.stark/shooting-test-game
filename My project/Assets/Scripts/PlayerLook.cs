using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerLook : TouchMove
{
    private Vector3 origRot;
    private float sensetivity = 3f;

    private void Update()
    {
        if(Input.touchCount > 0)
        {
            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                origRot = Input.GetTouch(0).position;
            }else if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                Vector3 movedPos = Input.GetTouch(0).position;

                float x = FilterGyroValues(movedPos.x - origRot.x);
                RotateLeftRight(x * sensetivity);
                float y = FilterGyroValues(movedPos.y - origRot.y);
                RotateUpDown(y * -sensetivity);

                origRot = movedPos;
            }
        }
    }
    float FilterGyroValues(float axis)
    {
        float thresshold = 0.5f;
        if (axis < -thresshold || axis > thresshold)
        {
            return axis;
        }
        else
        {
            return 0;
        }
    }
}

