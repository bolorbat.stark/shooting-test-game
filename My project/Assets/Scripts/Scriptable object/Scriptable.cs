using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Enumeration;
using UnityEngine;

[CreateAssetMenu(fileName = "Gun", menuName ="Scriptable/AR")]
public class Scriptable : ScriptableObject
{
    public int ammo;
    public int magSize;
    public float reloadSpeed;
}
