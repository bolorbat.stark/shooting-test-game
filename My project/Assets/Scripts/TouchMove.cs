using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMove : MonoBehaviour
{
    [SerializeField] private Camera cam;
    public void RotateUpDown(float axis)
    {
        cam.transform.RotateAround(transform.position, transform.right, -axis * Time.deltaTime);
    }
    public void RotateLeftRight(float axis)
    {
        cam.transform.RotateAround(transform.position, Vector3.up, -axis * Time.deltaTime);
    }
}
