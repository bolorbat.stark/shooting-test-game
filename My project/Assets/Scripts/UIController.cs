using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI ammotext;
    [SerializeField] private TextMeshProUGUI magText;
    [SerializeField] private Scriptable scriptableObject;
    [SerializeField] private PickUpController pickupController;
    [SerializeField] private Image Crosshair;
    [SerializeField] private Image Shoot;

    private void Start()
    {
        Crosshair.enabled = false;
        Shoot.enabled = false;  
    }

    // Update is called once per frame
    void Update()
    {
        if(pickupController.equipped)
        {
            ammotext.text = scriptableObject.ammo.ToString();
            magText.text = scriptableObject.magSize.ToString();
            Crosshair.enabled = true;
            Shoot.enabled = true;
        }
        else
        {
            Crosshair.enabled = false;
            Shoot.enabled = false;
            ammotext.text = null;
            magText.text = null;
        }
        
    }
}
